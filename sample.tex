\documentclass[twoside,11pt]{article}

\usepackage{seminar, rawfonts}
\usepackage{amsthm, amsmath, amssymb, mathtools}
\usepackage{caption}


\usepackage{subcaption}
\usepackage[utf8]{inputenc}


\usepackage[style=numeric,backend=biber]{biblatex}
\addbibresource{sample.bib}
\usepackage{hyperref}

\usepackage{graphicx}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

% theorem environments                
%                                     
\newtheorem{prop}                      {Proposition}
\newtheorem{lemma}                     {Lemma}
\newtheorem{theorem}                   {Theorem}
\newtheorem{corollary}                 {Corollary}
\theoremstyle{plain}                  
\newtheorem{definition}                {Definition}


\seminarheading{Seminar}{Advances in Artificial Intelligence}{Winter 2023}{ }
\ShortHeadings{ORB-SLAM2}{Jacob Loth}
\firstpageno{1}

\begin{document}

\title{ORB-SLAM2: an Open-Source SLAM System for
Monocular, Stereo and RGB-D Cameras $($Report$)$}

\author{\name Jacob Loth \email jacob.loth@uni-ulm.de}

% For research notes, remove the comment character in the line below.
% \researchnote

\maketitle


\begin{abstract}
One of the main problems when building autonomous machines
is the retrieval of reliable positional data.
Equally important are reliable coordinates of obstacles to allow for safe and efficient navigation.
SLAM$($Simultaneous Localization And Mapping$)$ solves both problems:
While moving through space SLAM creates a dynamic map 
of the environment while estimating the position within this map. 
The paper "ORB-SLAM2: an Open-Source SLAM System for
Monocular" presents a novel real-time SLAM algorithm, 
that performs better or equal to SLAM systems available at the time. 
While its predecessor ORB-SLAM only supports single monocular cameras, ORB-SLAM2 also supports stereo and RGB-D cameras,
which increases the mapping and localization accuracy. 
The main contributions of the paper are the seamless integration of RGB-D and stereo cameras in an Open-Source Framework 
and a novel optimization of camera parameters from stereo and mono key points.
\end{abstract} 

\section{Introduction}
Spatial understanding of the environment is the basis for any rational movement.
As man, we constantly keep and update an internal map of our surroundings and our position through visual data from our eyes.
Likewise, robots receive visual data through camera sensors and need to develop a similar understanding, 
in order to perform the increasingly complex we like to automate.\\
Examples of this include delivery drones \cite{tiemann2018enhanced}, Mars rovers \cite{cheng2005visual} or vacuum robots \cite{lee2013embedded}.
Generally there are three components to map-based navigation \cite{filliat2003map}:
\begin{itemize}
    \item Map learning: Developing a world representation while exploring.
    \item Localization: Deriving a position inside the world representation.
    \item Path planning: Calculate a course of action to reach a goal.
\end{itemize}
SLAM systems are a natural way to solve the first two problems since they depend on each other.
In praxis, we try to pinpoint and constantly update the robot's position in relation 
to its environment based on video input while keeping track of obstacles and goals.
Other than simple monocular camera input, a wide array of sensors can be used for SLAM.
Especially the combination of multiple data streams can improve SLAM accuracy under difficult conditions.
ORB-SLAM is a perfect example of this. Currently, there are three ORB-SLAM iterations:
The original ORB-SLAM \cite{mur2015orb} only uses monocular camera input. ORB-SLAM2 \cite{mur2017orb} extends ORB-SLAM with RGB-D and stereo image support, 
while ORB-SLAM3 \cite{campos2021orb} adds an inertial measurement unit into the system.
Each version improves upon the last in both accuracy and robustness through additional sensor data.  
In this report, we want to show how ORB-SLAM2 works in detail and improves upon the original ORB-SLAM.
\subsection{Related work}
\subsubsection*{Keyframe-based SLAM}
In contrast to many earlier filter-based SLAM systems, ORB-SLAM is keyframe-based. 
Filter based systems use every frame to update the map and camera parameters in one pass \cite{strasdat2012visual}. 
They mainly encode previous frames with probability distributions and try to minimize the uncertainty. 
One example of a filter-based system is SPMap \cite{castellanos1999spmap}, 
it uses a kalman filter to iteratively estimate the global slam system state. 
The main drawbacks of filtering SLAM systems are the accumulation of errors and performance problems under real-time conditions
because updates on large maps can be costly \cite{strasdat2012visual}.\\ 
Keyframe-based systems on the other hand avoid this by splitting map and position updates into separate tasks and only using a few important keyframes for mapping \cite{mur2015orb}. 
This allows for real-time positional updates and asynchronous expensive global map updates. 
Hence, keyframe-based systems are more suited to real-time applications and allow for optimal mapping.\\
Instead of using raw pixel data, ORB-SLAM preprocesses input images with the ORB $($Oriented FAST and Rotated BRIEF$)$ feature detector and extractor. 
ORB searches the input image for sharp edges and turns them into a set of feature points. 
Each point has an ID which is largely rotation, scale and illumination invariant \cite{rublee2011orb}. 
Thus, this ID allows tracking edges and movement over multiple frames.\\
ORB is used over other more accurate extractors and detectors like SIFT \cite{lowe2004distinctive} 
because it can run in real-time on small embedded systems and has decent accuracy.\\
At the core of ORB-SLAM is bundle adjustment $($BA$)$ optimization.
Given a set of interconnected feature points BA can accurately reconstruct both camera localization and mapping 
by minimizing the reprojection error of seen features over all frames \cite{triggs1999bundle}.
Bundle adjustment over many frames is expensive and was first only used in offline applications. 
One of the first keyframe-based SLAM systems to use BA was PTAM $($Parallel
Tracking and Mapping $)$ \cite{klein2007parallel}. 
PTAM splits camera tracking and mapping 
into separate threads and uses BA asynchronously inside the mapping threads.
ORB-SLAM builds on this concept and introduces three threads with BA: tracking, local mapping and global mapping and loop closing.\\
\subsubsection*{Loop Closing}
Loop closing is another important aspect of SLAM systems.
When a previously mapped location is revisited the robot moved in some kind of loop.
Since in the real world most locations are unique the accumulated error is the difference between the current and previous SLAM state.
Or in other words after moving in a circle we expect everything $($and ourselves$)$ to be in the same place as before.
To detect a loop a SLAM system has to recognize previously seen places. 
The work of Ethan Eade and Tom Drummond \cite{eade2008unified} pioneered the idea of using a bag of words database for loop closing. 
Here image features are discretized into a set of binary visual words with an offline-generated cluster database.
So similar images have similar word counts because of predefined visual patterns. 
ORB-SLAM uses a bag of words algorithm optimized for ORB \cite{mur2014fast} that is based on \cite{galvez2012bags}.
This technique is also used to reorient after the tracking is lost due to violent movement or view obstructions.\\

\subsubsection*{Monocular, Stereo and RGB-D}
Since the original ORB-SLAM only supports monocular input it has to deal with scale drift and a difficult initialization. 
In monocular SLAM systems, there is no way of knowing where initial 3D map points are from just a single image. 
Hence, some SLAM systems like \cite{klein2007parallel} require an explicit manual calibration phase to create the initial map from multiple images.
The original ORB-SLAM delays the initialization until suitable images with high parallax are detected and then uses epipolar geometry to estimate depth.
After initialization monocular systems suffer from scale drift because new map points often only have inaccurate depth estimations. 
So the depth and consequently scale may change \cite{mur2015orb}. 
RGB-D and stereo SLAM systems do not suffer from these issues because there is accurate depth information. \\
While stereo image disparity can be used to estimate depth, many earlier SLAM systems only support either stereo or RGB-D input.
An example of an early stereo SLAM system with good performance is the work by Paz et al. \cite{paz2008large}. 
An important contribution of this work is the separation and usage of true stereo features and degenerate monocular features without disparity. 
A more recent example is LSD-SLAM by \cite{engel2015large}, it uses stereo images directly to minimize the photometric error.\\
One famous RGB-D SLAM system is kinectfusion \cite {newcombe2011kinectfusion} it uses the kinect 
sensor and a dense volumetric mesh map representation. Sadly the representation limited usage to small workspaces.
Elastic Fusion SLAM builds on this concept and uses a voxel based map \cite{whelan2015elasticfusion}. The map size is 
kept low through a sophisticated culling algorithm. 
It also estimates the camera pose directly from the map, 
so there is no need for a pose graph.
Another way to use RGB-D is the conversion of RGB-D features to stereo features \cite{strasdat2011double}, 
this removes the need for matching left and right features as required for direct stereo inputs. 


\section{Theoretical Framework}
\begin{figure}[t]
    \centering
    \includegraphics[width=12cm]{ horizon.png}
    \caption{A depiction of close(green) and far(blue) ORB features. \cite{mur2017orb}. }
    \label{fig:ba}
\end{figure}

\subsection{RGB-D to Stereo}
ORB-SLAM2 expects calibrated stereo input where all camera properties are constant and known.
A calibrated stereo key point is defined as $X_S=(x_L,y_L,x_R)$. 
The first two coordinates represent the position in the left image while the third coordinate is a horizontal offset in the second image. 
Finding $u_R$ from an arbitrary $x_L,y_L$ can be done efficiently using calibrated stereo matching.
A RGB-D key point $X_{D}=(x_L,y_L,d)$ maybe converted to stereo using:
\begin{equation}
    x_R = x_L-\frac{f_xb}{d}
\end{equation}
Where $f_x$ is the focal length and $b$ the distance between the depth infrared raster and infrared sensor.
This allows for a unified RGB-D and stereo interface.
Since distant stereo points can be inaccurate there is a distinction between: \textbf{close} and \textbf{far} points.
Close points are accurate, so they can be triangulated directly using epipolar geometry.
Far points on the other hand, are triangulated from multiple viewpoints.
The decision when points are far and close depends on the camera and $u_R$ and is made experimentally. 
Furthermore, there can be degenerate key points when stereo matching fails or an RGB-D point has an invalid depth.
When multiple frames support a degenerate feature point they are triangulated and used as monocular key points.
Monocular key points assist in rotation and translation estimation only, because of scale drift.
\subsection{Bundle Adjustment}
\subsubsection{Monocular Bundle Adjustment}
BA minimizes the reprojection error 
over a set of frames and estimated camera and map coordinates. For an example of this see figure \mbox{\ref{fig:ba}}.
Featurepoints that are tracked over multiple frames create dependencies 
between these frames and requirements for possible camera and map coordinates. 
Numerically this results in a large set of non-linear equations that can be solved through least-sequares optimization. 
Since only some key points are visible per frame the equation system has many zeros and can be solved efficiently.  
On the other hand careful keyframe selection is necessary in order to not introduce to many small dependencies.
\begin{figure}[t]
    \centering
    \includegraphics[width=8cm]{ba.png}
    \caption{Traditional monocular BA, where $u_{ij}$ are feature points, $C_j$ are camera matrices and $X_i$ are map points while $i$ and $j$ are the observation and map point index respectively. 
    $u'_{ij}$ are reprojections of map points according to the camera, 
    the dotted lines show the reprojection error \cite{chen2019bundle}. }
    \label{fig:ba}
\end{figure}\\
Formally BA is a non-linear optimization of keyframe matrices $R_j\in SO(3)$ and positions $t_j \in R^3$ and map points $X_i\in R^3$ 
with respect to feature points $u_{i}\in R^2$ and their reprojections $u'_{i} \in R^2$.
The cost function can be expressed as:
\begin{equation}
    C = \sum_{i,j}\rho(\norm{u_i-\pi (R_jX_i+t_i)}^2_{\Sigma}   )
\end{equation}
Where $\rho$ is the robust Huber cost function to avoid outliers and $\Sigma$ the scale covariance matrix of all $u_{i}$.
$\pi$ is the reprojection function of a map point defined as:
\begin{equation}
    \pi:\begin{bmatrix}X\\Y\\Z\\ \end{bmatrix} \to \begin{bmatrix} f_x \frac{X}{Z}+c_x\\f_y\frac{Y}{Z}+c_y \end{bmatrix}
\end{equation}
With intrinsic camera properties: focal point $f_x,f_y$ and principal point $c_x,c_y$.\\
Another important attribute of BA is its flexbility. First BA can optimize only a subset of keyframes and map points by ignoring the rest, this is useful for local optimization.
It secondly supports tracking only optimization where map points are constants, this is much faster than full BA since the number of parameters is way lower.\\
ORB-SLAM and ORB-SLAM2 use the Levenberg–Marquardt method
implementation from g2o \cite{kummerle2011g} to solve BA.
\subsubsection{Stereo Bundle Adjustment}
The stereo BA proposed by the authors adds a distinction between stereo and mono key points.
Each key point $u_i^{(.)}$ is either stereo $u_i^s=(x_L,y_L,x_R)$ or mono $u_i^m=(x_L,y_L)$.
Additionally to the monocular reprojection function $\pi_m$ a new stereo reprojection function $\pi_s$ is defined as:
\begin{equation}
    \pi_S:\begin{bmatrix}X\\Y\\Z\\ \end{bmatrix} \to \begin{bmatrix} f_x \frac{X}{Z}+c_x\\f_y\frac{Y}{Z}+c_y\\ f_x \frac{X-b}{Z}+c_x  \end{bmatrix}
\end{equation}
With these changes the new stereo cost function becomes:
\begin{equation}
    C = \sum_{i,j}\rho(\norm{u_i^{(.)}-\pi_{(.)} (R_jX_i+t_i)}^2_{\Sigma}   )
\end{equation}
\subsection{Similarity transformations and Loop Closing}

\label{sec:sim}
When keyframe $K_1$ and $K_2$ contain 
mostly similar features they could represent a loop.
To decide, we have to analyze the geometrical relation between the two frames.
In the case of monocular SLAM other than rigid body 
transformations there can also be scale drift. 
So the difference between two frames must be described with a similarity transformation in $SIM(3)$.
A similarity transformation includes 7 degrees of freedom(DOF), 
rigid body transformation and scale. 
For stereo/RGB-D SLAM 6 DOF are enough 
But since ORB-SLAM2 also includes monocular keypoints,
loops are solved using similarities. To determine a similarity transformation between frames we have to look at the map points.
For each shared feature $f_k, k<n$ there is a map point match $X_i=>X_j$ and a feature match $u_i=>u_j$
An optimal similarity $S_{12}$ minimizes the reprojection error of all matches in both images.
So we  have the cost function:
\begin{equation}
    C = \sum_{n}\rho(\norm{u_i^{(.)}-\pi_{(.)} (S_{12}(X_i))}_{\Sigma})+\rho(\norm{u_j^{(.)}-\pi_{(.)} (S_{12}(X_j))}_{\Sigma}  )
\end{equation}
Where $\Sigma$ is the covariance of individual keypoints in their respective frames.
This function can be solved using the same method as in BA.
With the $S_{12}$ we can then merge $K_1$ with $K_2$.
Afterwards the changes have to be propagated to all other frames.
One option here is a full costly BA, 
but there is a simpler approximation.
First camara absolut transforms are converted to similarity transforms with scale 1.
The error between to two camera frames $S_i,S_j \in SIM(3)$ is defined as 
\begin{equation}
    e_{ji}= S_{ij}S_iS_j^{-1}
\end{equation}
Where $S_{ij} \in SIM(3)$ is the rigid body transformation from $S_j$ to $S_j$ before the loop closure as a similarity transformation with scale 1.
Only the scamera matrix of $S_{1}$ has the scale of $S_{12}$ so scale dirft is propagated correctly.
$e_{ij}$ is to a least-sequares problem using change of basis into the 7-DOF tangent space in $\mathbb{R} ^7$ for fast solving \cite{strasdat2011scale}.
This essentially spreads the loop correction error across frames.







\newpage
\section{Implementation}
\begin{figure}[t]
    \centering
    \includegraphics[width=15cm]{overview.png}
    \caption{The main ORB-SLAM2 threads: tracking, local mapping  loop closing and shared data structures: the map representation and place recognition. On the left the preprocessing into stereo keypoints.  \cite{mur2017orb}. }
    \label{fig:ov}
\end{figure}
As show in figure \ref{fig:ov} ORB-SLAM2 is split into three threads and two central components.
The actual implementation is done in an C++ open-source library\footnote{https://github.com/raulmur/ORB\_SLAM2}.
Many hyperparameters in the system are determined experimentally, 
so terms like "when there are {\bf enough}  y" refer to experimentally determined constants.
In the following we will examine the most important components and how they interact.
\subsection{Central Data Structures}
As shown in figure \ref{fig:str}
the map representation consists of 4 parts:
\begin{itemize}
    \item MapPoints: A pointcloud of 3D map points representing the known environment.
    \item KeyFrames: A set of feature sets that are important because they contain view or map changes.
    \item CovisibilityGraph: A undirected weighted graph over the keyframes, two frames are connected if they share enough features. 
        The weight is relative to the number of shared features. 
    \item SpanningTree: A incremental spanning tree from the initial node over the covisibility graph with the minimal number of edges and the highest weight.
        The spanning tree is used to build a new essential graph. That only has "important" connections between keyframes.  
        And yields a strong network of camera frames.
\end{itemize}
\begin{figure}
    \centering
\begin{subfigure}[t]{.22\textwidth}
  \centering
  \includegraphics[width=\linewidth]{keyframes.png}
    \caption{}

  \label{fig:sfig1}
\end{subfigure}%
\begin{subfigure}[t]{.22\textwidth}
  \centering
  \includegraphics[width=\linewidth]{covis.png}
    \caption{}

  \label{fig:sfig2}
 \vskip\baselineskip
\end{subfigure}
\begin{subfigure}[t]{.22\textwidth}
  \centering
  \includegraphics[width=\linewidth]{stree.png}
    \caption{}

  \label{fig:sfig3}
\end{subfigure}
\begin{subfigure}[t]{.22\textwidth}
  \centering
  \includegraphics[width=\linewidth]{essential.png}
  \caption{}
  \label{fig:sfig4}
\end{subfigure}

    \caption{The ORB-SLAM map components. 
        Figure (a) shows the visible map points(red) for the active camera frame(green) other camera frames are blue. 
        The other images show the covisibility graph, spanning tree, and essential graph respectively \cite{mur2015orb}.}

\label{fig:str}

\end{figure}
\begin{figure}[t]
    \centering
    \includegraphics[width=11cm]{bags.png}
    \caption{A DBoW2 database. 
        The vocabulary tree consists of three layers, 
        the leaves are learned words other nodes are learned word clusters.
        When a new image is inserted its features are used as binary words $w_i$.
        Each feature word traverses the tree always choosing the closest child according to the hamming distance. 
        Finally, a visually similar leaf word is picked for each feature. 
        If done for all features an image-dependent bag of words count vector $v$ is created, from the image feature indices.  
        Two vectors are compared using the similarity weight $s(v1,v2) = 1-\frac{1}{2}\abs{\frac{v_1}{\abs{v1}}-\frac{v2}{\abs{v2}} }$.
        Using the inverse index for each word it is possible to quickly find similar images because only images with similar 
        words have to be compared \cite{galvez2012bags}.
    }
    \label{fig:bag}
\end{figure}
The place recognition database is the second central component of ORB-SLAM2. 
It uses a DBoW2 \cite{galvez2012dbow2} visual vocabulary tree to create visual words from keyframes. 
A database stores which visual words are used by which keyframes, 
so image lookup is efficient. Figure \ref{fig:bag} shows this process in more detail. 
While active  the database is constantly updated when a new keyframe is created or deleted.
\subsection{Tracking}
New images are forwarded to the tracking thread. 
It deals with preprocessing, camera movement and decides what frames are keyframe worthy.
Preprocessing transforms input images into stereo or mono keypoints with ORB and stereo keypoint conversion.
New keyframes are then tracked, which means finding shared features between the current and some set of previous frames. 
There are three search cases:
\begin{itemize}
    \item In most cases it is enough to find the shared features of the current frame.
    \item If there aren't enough shared features the search is widened to the last few frames.
    \item If both previous searches fail the place recognition database is used for a global search. 
\end{itemize}
After the connected frames are found, motion-only BA is used to create an initial camera frame.
This in turn can be used to retrieve all visible map points, and their connected keyframes.
Then keyframes with shared map points and their direct neighbors in the covisibility graph 
are utilized to optimize the camera frame again.
With the newly optimized camera frame, 
tracking has to decide if it should send the keyframe to the local mapping thread.
A new keyframe is created if all following conditions are met:
\begin{itemize}
    \item More than 20 frames have passed since the last local mapping and global relocalization pass.
    \item The current frame has more than 50 map points.
    \item The current frame tracks less than 90\% of map points than its neighboring keyframe with the most similar features.
\end{itemize}
The first condition protects against too frequent updates, while the next two ensure only frames with enough information and changes are created.
Additionally, a new keyframe is always created if there are 
less than 100 close stereo keypoints and the new keyframe will insert at least 70 new close stereo keypoints.
This helps when there is a lack of accurate depth information.

\subsection{Local Mapping}
The local mapping thread inserts new keyframes, creates  and optimizes new map points, and culls keyframes.
Once a new keyframe is sent from tracking, 
it is first inserted into the covisibility graph and place recognition database. After which the spanning tree is updated.
Next, new map points are triangulated. Close stereo points are triangulated immediately, 
while far and degenerate features need multiple observations to support them. 
Thereafter, local BA optimizes the current keyframe and all its neighbors in the covisibility graph.
To keep the keyframe count as low as possible local mapping tries to aggressively cull existing keyframes.
When a keyframe has 90\% map point similarity with at least 3 other frames it is deleted.
\subsection{Loop Closing and Global Optimization}
The first major step of loop closing 
is loop detection. After tracking sends a newly accepted keyframe $K_i$ 
which is found in the place recognition database there is a potential loop candidate. 
To make sure the loop detection is robust, all neighbors of $K_i$ in the covisibility are ignored. 
Additionally, every loop candidate has to share at least as 
many features with $K_i$ as its least similar neighbor in the covisibility graph.
If a suitable loop candidate $K_j$ is found a optimal transformation $S_{ij} \in SIM(3)$ between 
the $K_i$ map points and $K_j$ map points is calculated as described in \ref{sec:sim}. 
To reduce unnecessary optimization, multiple starting similarities from key point pairs are generated. 
Optimization is only applied if there are enough mapoint inliers.
Since there have to be 
close map point to map point correspondences this step again culls loop candidates 
if there are too few.
If a transformation has enough shared map points the loop is accepted.
Now the loop can be fused.
$S_{ij}$ is first applied to $K_i$  after which 
the change propagated to neighbors in the covisibility graph.
Then both keyframes are fused. 
Since there now exist duplicate keypoints, all map points visible from $K_1$ are also fused.
Next, the error is propagated along the essential graph to smooth the loop edges as shown in \ref{sec:sim}.
The final optimization is an asynchronous full BA, when it finishes the changes are 
applied back to the global map. Another parallel loop closure restarts the global BA.
\newpage

\section{Evaluation}
\begin{figure}
    \centering
\begin{subfigure}[t]{.33\textwidth}
  \centering
  \includegraphics[width=\linewidth]{kittiq.png}
    \caption{}

\end{subfigure}%
\begin{subfigure}[t]{.33\textwidth}
  \centering
  \includegraphics[width=\linewidth]{euroc.png}
    \caption{}

 \vskip\baselineskip
\end{subfigure}
\begin{subfigure}[t]{.33\textwidth}
  \centering
  \includegraphics[width=\linewidth]{rgbd.png}
    \caption{}

\end{subfigure}

    \caption{Quantitative results of different SLAM systems on the three datasets: KITTI(a), EuRoC(b), TUM RGB-D(c)}

\label{fig:q}
\end{figure}

ORB-SLAM2 is tested on 3 popular datasets to compare its capabilities with other SLAM systems available.
The datasets include:
\begin{itemize}
    \item KITTI: an automotive stereo dataset
    \item EuRoC: an aerial stereo dataset from small drones
    \item TUM RGB-D Dataset: indoor RGB-D video sequences under varying conditions.     
\end{itemize}
All datasets include ground-truth tracking data to evaluate SLAM performance. 
All tests are run on an Intel Core i7-4790 with 16GB of ram. 
Each is repeated 5 times and median selected to avoid outliers.
For the stereo datasets ORB-SLAM2 is compared to LSD-SLAM  \cite{engel2015large} as the state-of-the-art.
While for the RGB-D dataset Elastic Fusion \cite{whelan2015elasticfusion},
Kintinuous \cite{whelan2012kintinuous}, DVO-SLAM \cite{yan2017dense} and RGB-D SLAM \cite{endres20133} are chosen for comparison.
To judge system accuracy the root-mean-sequare error (RMSE) of the predicted translations and ground truth in meters is used.
For the KITTI dataset the relative translational and rotational errors are also presented.
In general, ORB-SLAM2 manages to beat all other SLAM systems in most tests. Furthermore it also only loses tracking once in the EuRoC V2\_03\_diffcult sequence.
This is due to violent motions and motion blur in the test sequence.This 
Another notable fact is that ORB-SLAM fails on one of the KITTI sequences while ORB-SLAM2 does not.
This shows that the additional depth information yields greater robustness.
In the RGB-D dataset ORB-SLAM also performs better than the rest. 
Hence, the conversion from RGB-D to stereo does not limit the system's effectiveness.
\newpage

\subsection{Runtime Performance}
\noindent%
\begin{minipage}{\linewidth}% to keep image and caption on one page
\makebox[\linewidth]{%        to center the image
  \includegraphics[width=8cm]{speed.png}}
\end{minipage}
As additional analysis the authors provide 
the mean timing for different stages of ORB-SLAM2.
This shows that the algorithm can run in real-time on consumer hardware.
Though it has to be said that the used processor was very powerful at the time.
\section{Discussion}
ORB-SLAM2 is a sensible extension of the original ORB-SLAM, 
with improved performance when better sensors are available.
Especially the combination of RGB-D, stereo and mono features 
in a single BA algorithm make it remarkable. The system component
structuring and parallelization also seem very natural for SLAM problems.
Another great property for real world applications are 
the low system requirements. These make it possible to utilize ORB-SLAM2 in many areas of robotics.
Like the monocular ORB-SLAM, the evaluation shows better performance than other state-of-the-art SLAM systems in most tests. 
Which hints that the components and structure of ORB-SLAM2 solve SLAM problems well, regardless of sensor input.
But there are also tests in the EuRoC dataset 
that all SLAM systems including  ORB-SLAM2 have failed.
This shows that there is still work to be done.
There are essentially two  ways to improve ORB-SLAM2 from here.
One is the extension with more sensor data, as done in ORB-SLAM3.
The other is an improvement of the overall system with new components.
BA as the core of ORB-SLAM is very hard to improve and very performs well.
A good starting point for change might be the ORB descriptor. With the recent improvements 
in deep learning a new type of descriptor and detector has emerged.
SuperPoint is deep learning driven framework for effective feature detection and description, 
with a higher robustness and accuracy than ORB. While the performance is similar at least on GPUs \cite{detone2018superpoint}.
There is also work on making this technology less GPU dependent
\cite{maurer2022zippypoint}, so it can run normal CPUs like ORB.
A similar point of improvement might be the place recognition database, since the used method is also dated.
To conculude, ORB-SLAM2 combines different technologies into a robust SLAM system. 
It performs better than other SLAM systems but has great potential for further improvements. 









%% diesen Seitenumbruch dürfen sie _nicht_ entfernen
\newpage
\appendix
\vskip 0.2in
\printbibliography
\end{document}






